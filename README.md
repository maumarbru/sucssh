# Sumple Unix Chat with SSH

The [Dockerfile](Dockerfile) generate an Debian based image which includes:
- Build essentials.
- [suc](https://gitlab.com/edouardklein/suc) with some modification, builded and installed during Docker building.
- The `sshd` installed. The default command for this image is to run `sshd`.

After running this image you have an `sshd` service encapsulated inside a Docker container, exposing the port 11044.

## Build and start the service

```sh
make build start logs
```
to build the docker image, start a persistent service and show the logs. To exit logs just Ctrl-c. The service will
continue running.

### Create a new user with SSH access
```sh
make shell
addsshuser.sh
```
and follow the intructions.

> Use only ed25519 keys. You can generate it by `ssh-keygen -t ed25519`.


### Enter

```sh
ssh localhost -p 11044
```
with your user.
