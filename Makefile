image := sucssh
container := sucssh
host := sucssh

docker_run := docker run -it \
  --name $(container) \
  --hostname $(host) \
  -v $(PWD)/.home:/home \
  -e USER=root \
  -p 11044:22

.PHONY: build
build:
	@docker build -t $(image) .


.PHONY: start
start:
	@$(docker_run) -d --restart unless-stopped $(image)


.PHONY: restart
restart:
	@docker restart $(container)


.PHONY: logs
logs:
	@docker logs -f --tail=1000 $(image)


.PHONY: shell
shell:
	@docker exec -it $(container) bash


.PHONY: clean
clean:
	@docker stop $(container)
	@docker rm $(container)


.PHONY: cleanall
cleanall: clean
	@docker rmi $(image)
