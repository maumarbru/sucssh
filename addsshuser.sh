#!/usr/bin/bash
set -euo pipefail

read -p "username: " -r user || exit 0
read -p "ssh-ed25519 pubkey: " -r pubkey || exit 0
useradd --create-home --shell /bin/bash $user || true
usermod -a -G suc $user
home=/home/$user
chmod o-rwx /home/$user
su $user << EOF
mkdir -p $home/.ssh
echo "$pubkey" >> $home/.ssh/authorized_keys
EOF


