FROM debian:latest

RUN export DEBIAN_FRONTEND=noninteractive \
    && apt-get -qq update \
    && apt-get -qq install --no-install-recommends \
    build-essential \
    git \
    vim \
    lnav \
    python3-pygments \
    bat \
    rlwrap \
    sudo \
    ssh

COPY suc /root/suc
WORKDIR /root/suc
RUN make suc install

WORKDIR /root

RUN mkdir /run/sshd
COPY sshd_config /etc/ssh/sshd_config
COPY .bashrc /root/
COPY addsshuser.sh /usr/bin/
COPY suc/suc_channel.sh /usr/bin/

RUN ln -s /usr/bin/python3 /usr/bin/python

CMD /usr/sbin/sshd -Dd 

